Projektkontrolle/-risiken - PräInstruktion. 
(Jeder für sich, schriftlich.  Vermutungen sind auch gut)

1.) Welche grundsätzlichen Risiko-Dimensionen gibt es?

2.) Was machen sie, wenn die Tragweite des Risikos hoch ist? 

3.) Was ist eine Risikoanalyse und weshalb wird diese durchgeführt? 

4.) Was machen sie, wenn die Eintrittswahrscheinlichkeit eines Risikos hoch ist? 

5.) Wann ist der geeignete Zeitpunkt, um eine Risikoanalyse durchzuführen? 
