Planung - Vorwissenaktivierung/PräInstruktion. 
(Jeder für sich, schriftlich. Vermutungen sind auch gut)

1.) Was ist ein Projekt?

2.) Ist alles was man macht ein Projekt?

3.) Wer gehört typischerweise zu einem Projekt?
    (und wer nicht?)

4.) Wo habe ich schon Projekte angetroffen 
    oder wer sagte schon mal was über ein Projekt?

5.) Wie kann ich ein Projektziel formulieren?

